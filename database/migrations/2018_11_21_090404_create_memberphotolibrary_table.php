<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberphotolibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberphotolibrary', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photoname');
            $table->string('phototitle');
            $table->longText('photodescription');
            $table->string('member_id');
            $table->string('photocategories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberphotolibrary');
    }
}
