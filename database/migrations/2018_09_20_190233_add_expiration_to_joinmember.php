<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpirationToJoinmember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('JoinMember', function($table) {
            $table->string('approved_date')->nullable();
            $table->string('expiration_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('JoinMember', function($table) {
            $table->string('approved_date')->nullable();
            $table->string('expiration_date')->nullable();
        });
    }
}
