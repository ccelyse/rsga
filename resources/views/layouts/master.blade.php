<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />
    <script>function sf_writeCookie(){the_cookie=document.cookie,the_cookie&&window.devicePixelRatio>=2&&(the_cookie="sf_pixel_ratio="+window.devicePixelRatio+";"+the_cookie,document.cookie=the_cookie)}sf_writeCookie();</script>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com' />
    <link rel='dns-prefetch' href='http://s.w.org' />

    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <link rel="apple-touch-icon" href="images/RSGAlogo.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/RSGAlogo.png">
    <link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Feed" href="feed/index.rss" />
    <link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Comments Feed" href="comments/feed/index.rss" />
    <script src="cdn-cgi/apps/head/_jZ0jIuSj0S4kK9BleaWviaKHSc.js"></script><script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
    <link type="text/css" media="all" href="stylesheet/autoptimize_7dd880bed0f2d2c4c082021c6fc97535.css" rel="stylesheet" />
    <!--    <link type="text/css" media="all" href="wp-content/cache/autoptimize/1/css/autoptimize_7dd880bed0f2d2c4c082021c6fc97535.css" rel="stylesheet" />-->
    <link type="text/css" media="only screen and (max-width: 768px)" href="css/autoptimize_3be592c671f598b43e637562b04345ed.css" rel="stylesheet" />
    <link type="text/css" media="only screen and (max-width: 768px)" href="fontawesome/all.css" rel="stylesheet" />
    <link rel='stylesheet' id='redux-google-fonts-sf_uplift_options-css' href='css/lato.css' type='text/css' media='all' />
    <script type='text/javascript' src='js/jquery/jquery-ver=1.12.4.js'></script>
    <script type='text/javascript' src='fontawesome/all.min.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min-ver=1.4.1.js'></script>
    <script type='text/javascript' src='js/combine/plyr.js'></script>
    <link rel='https://api.w.org/' href='wp-json/index.json' />

    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php-rsd.xml" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <link rel="canonical" href="" />
    <link rel='shortlink' href='' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed-url=http-%7C%7Cuplift.swiftideas.com%7C.json" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed-url=http-%7C%7Cuplift.swiftideas.com%7C&amp;format=xml.xml" />
    <!--[if lt IE 9]><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/respond.js"></script><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/html5shiv.js"></script><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/excanvas.compiled.js"></script><![endif]-->
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
</head>
<body class="home page-template-default page page-id-9 page-parent  minimal-design mobile-header-center-logo mhs-tablet-land mh-sticky  mh-overlay responsive-fluid sticky-header-enabled page-shadow mobile-two-click standard product-shadows header-naked-light layout-fullwidth has-quickview-hover-btn disable-mobile-animations ">
@yield('content')