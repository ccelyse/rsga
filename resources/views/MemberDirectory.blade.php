@extends('layouts.master')

@section('title', 'RSGA')

@section('content')

    @include('layouts.topmenu')
<style>
    .sf-list li {
        margin-bottom: 6px;
        line-height: 200%;
        text-transform: uppercase;
    }
</style>
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/A gorilla family in Volcanoes National Park.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style=""></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">MEMBERS DIRECTORY</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width col-sm-12 hidden-xs col-natural" data-row-style="" data-v-center="false" data-top-style="none" data-bottom-style="none" style="margin-top: 0%!important;margin-left: 0%!important;margin-right: 0%!important;margin-bottom: 0%!important;border-top: 0px default !important;border-left: 0px default !important;border-right: 0px default !important;border-bottom: 0px default !important;padding-top: 0%!important;padding-left: 0%!important;padding-right: 0%!important;padding-bottom: 0%!important;background-color:undefined!important;">
                                    <div class="spb_content_element" style="">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                                {{--<p>The law and ministerial orders regulating the operations of all tourism entities in the country, including accommodation establishments, restaurants, bars, nightclubs, tour operators, travel agencies, tour guides, tourism information offices, cultural tourism entities (cultural villages, private museums) and any other entity as may be determined by Order of the Minister. </p>--}}
                                                <ul class="sf-list ">
                                                    @foreach($listcat as $data)
                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>{{$data->firstname}} {{$data->lastname}}</span></li>
                                                    @endforeach
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KIRENGA Kamugisha</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BAHIZI Arthur</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUYENZI Martin</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KABALISA .J.Paul</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Kwizera Patrick</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MAGEZI.J.S</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NDIZEYE,P.Suleiman</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>UWASE Anuarite</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MANZI ERIC</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MURASANYI Karim</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUHIRE Jeremiah</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>RUTAYISIRE Joseph</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NSHABAMUNGU Nuuru</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KAYIRANGA Eric</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KINYANZA R Eric</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NIZEYIMANA.P. Ngabo</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NKUSI Guillaume</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KARANGWA Moses</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUTEZINTARE Hussein</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KALISA TUMAINI</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>GAHONGAYIRE.J. Bosco</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HAKIZIMANA Yasin</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BILIMA Jimmy</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUGABE Gilles</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HAKIZAMUNGU.A.J. Claude</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BAHINTASI Eugene</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KAMUGISHA Elliot</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>SIBOMANA Emmanuel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BIZIMANA Damien</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>TUMUSIME Charles</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>TANGANYIKA Sam</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HABIYAMBERE Innocent</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MANIRIHO Augustin</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUGABO MOSES</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KARANGWA Emile (50K Prepaid)</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>RUGANINTWARI Paul</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>TWAMBAZE .J.M Vianney</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MANIRAKIZA ERIC</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HAGENIMANA MANSOOR</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Lucian</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Nzabonimpa Theodore</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>GISIMBA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>SAFARI SEGATARAMA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>VALENS KALISA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HENRY  MWUMVANEZA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BYIMANA CLEMENT</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KATARABIRWA J.DE DIEUX</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NSINGA OLIVIER</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>OMAR GASIGWA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>ANDERSON MWESIGYE</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>ISAAC NGARAMBE</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>RULINDA EMMANUEL</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>GAKARAMA MAKINDI</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NTABANGANYIMANA ANACLET</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>RUTEMBESA THEOGENE</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>TEGA AMOS</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KAYITARE DEOGRATIAS</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUSAFIRI ISAAC</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>RINGUYENEZA PROTAIS</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUSONI INNOCENT</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KADIDI Kenneth</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KASIMU</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NIYONGIRA MAGNIFIQUE</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>FRED MUNGUMOJA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KAGABA ALEX</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUNYAO Bramwel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Kayitana jean bosco</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Kalinda Andrew</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BARANYERESTE</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KAMANZI Emmanuel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>AMON KAGURUBE</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUTABAZI Fabrice</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KIRENZI MOSES</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>GASANA Samuel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Uwimana NZAMWITA</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>SINZINKAYO Eastache</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MWEDEMEZI Elias</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KABATSI j. Marie</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>GAKUBA Fred</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUREKEZI Theogene</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BYARUGABA Duncan</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NGOBOKA SERAGO</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>NKURANGA Moses</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MUSEMINARI Innocent</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ally SADDy</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Frank Kanamugire</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Mukwaya Rutsobe</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Karamuka Mussa</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Nshimiyimana Jean De Dieu </span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Rutayitera Patrick</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Byiringiro Nathan</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ntagungira Yves</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Muhire Fidele</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Olivier Nishimwe </span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Nduwayezu Jean Claude</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Munyankindi Bernard</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Munyawera Swaib</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ian Iwacu</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Insha-allah</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Semusatsi Issa</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Mutijima Junior</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ntwari Gabriel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Theogene Evady Gash</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Nasasira Peter</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Kalasira Ronald</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Kaberuka Kaitan</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Uwayo Honorine</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ndahiro Emmy</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ishimwe Sharif</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Pamela Giramata</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Masengesho Emmanuel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ndisanze Ferdinand</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Uwiragiye Pierre</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ronald Mugisha</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Hakorimana J Bosco</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Rutayisire Jean Claude</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Gahigiro Jean Claude</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Murindwa Didier</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Mbabazi Francois Regis</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>ISHIMWE Pacifique</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Mpano Daniel </span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HABINEZA Jacques</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Nshuti Jean de Dieu</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BUCYANA Jean Paul</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Peter SAFARI</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>URIHO Yvan</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MWIZERWA Jean Paul</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BIZIMANA Eric</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>UMURUNGI Ruth</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HIRWA Christian</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>BUCYE DAVID</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KWIZERA Emmanuel</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>OMBENI MUKUNZI J BOSCO</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>KANYABIKALI Olivier</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>MBONABUCYA KASSIM</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Manirakiza Theogene</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Kirenga Aisha Ingabire</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Ngenzi Christian</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Luck Mboneza</span></li>--}}
{{--                                                    <li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>Hategekimana Jean Claude</span></li>--}}
                                                    {{--<li class="col-md-4"><i class="sf-icon-right-chevron"></i><span><strong><a href="" style="color:#6a442b !important;font-weight: bold;" target="_blank"></a></strong>HOTEL</span></li>--}}
                                                </ul>
                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
    @include('layouts.footer')
@endsection