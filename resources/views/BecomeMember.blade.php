@extends('layouts.master')

@section('title', 'RSGA')

@section('content')

    @include('layouts.topmenu')
    <style>
        .help-block{
            color: red !important;
        }
        .title-wrap h3 {
            color: #000 !important;
        }
        .spb-asset-content p {
            color: #000 !important;
        }
        .woocommerce form .form-row select {
            cursor: pointer;
            margin: 0;
            float: right;
            width: 75%;
        }
        .woocommerce form .form-row select {
            cursor: pointer;
            margin: 0;
            float: right;
            width: 60% !important;
        }
        .woocommerce form .form-row textarea{
            height: auto !important;
        }
        .woocommerce form #customer_details .form-row textarea {
            float: right;
            width: 60% !important;
            line-height: 20px;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/slider3.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0.5;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">MEMBERSHIP</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Membership benefits</span></h3>
                                            </div>
                                            @if (session('success'))
                                                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                    {{ session('success') }}
                                                </div>
                                            @endif
                                            @if ($errors->has('attachyourcv'))
                                                <span class="help-block">
                                                         <strong>{{ $errors->first('attachyourcv') }}</strong>
                                                    </span>
                                            @endif
                                            @if ($errors->has('attachyourdiplomas'))
                                                <span class="help-block">
                                                         <strong>{{ $errors->first('attachyourdiplomas') }}</strong>
                                                    </span>
                                            @endif

                                            @if ($errors->has('drivinglicense'))
                                                <span class="help-block">
                                                         <strong>{{ $errors->first('drivinglicense') }}</strong>
                                                    </span>
                                            @endif


                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>Capacity building for members through trainings such as, first aid, birding, consumer behavior and guiding ethics.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Advocacy and lobbying.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>RSGA members are trusted for service excellence and professionalism.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Networking and mentorship through peer to peer learning.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>RSGA helps its members to acquire their health insurance from affordable and qualified providers.</span></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Requirements and eligibility </span></h3>
                                            </div>
                                            <p>Joining RSGA has been made easy:<br>
                                            </p>
                                            <p><strong>1st stage:</strong>Submission of the duly filled application form with the below required documents</p>
                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>C.V (Detailed background of the applicant)</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Supporting documents( diploma, certificates, etc.)</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>A passport photo attached to the CV.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Driving licence (Optional)</span></li>

                                                <p><strong>2nd stage:</strong>The applicant will undergo an interview with the RSGA leadership team.</p>
                                                <p><strong>3rd Stage:</strong>The applicant is required to pay the membership and registration fees.</p>
                                            </ul>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Fee structure</span></h3>
                                            </div>
                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>Once off registration fee: 130,000 Rwf</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Quarterly membership fees (*4): 50,000 Rwf</span></li>
                                            </ul>
                                            <p>NB:Membership and registration fees have to be remitted to the bank account of RWASAGA: 000420693933660 (RWF) in Bank of Kigali</p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-7   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Apply for membership</span></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="woocommerce">
                                        <!--                                        <form name="checkout" method="post" class="checkout woocommerce-checkout row" action="scripts/applymembership.php" enctype="multipart/form-data">-->
                                        <form name="checkout" method="post" class="checkout woocommerce-checkout row" action="{{url('JoinMembers')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="container"></div>
                                            <div class="col-sm-10 col-md-offset-1" id="customer_details">
                                                <div>
                                                    <div class="woocommerce-billing-fields">

                                                        <div class="clear"></div>

                                                        <p class="form-row form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field">
                                                            <label for="billing_country" class="">Type of Membership</label>
                                                            <select class="country_to_state country_select billing_country" name="typeofmembership"  required>
                                                                <option value="{{ old('typeofmembership') }}">{{ old('typeofmembership') }}</option>
                                                                <option value="Association" selected>Association</option>
                                                                <option value="Champions (Indashyikirwa)">Champions (Indashyikirwa)</option>
                                                                <option value="District">District</option>
                                                                <option value="Golden Circle">Golden Circle</option>

                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field" >
                                                            <label for="billing_country" class="">Chamber</label>
                                                            <select class="country_to_state country_select billing_country" name="chamber" required>
                                                                <option value="{{ old('chamber') }}">{{ old('chamber') }}</option>
                                                                <option value="Chamber of Tourism" selected>Chamber of Tourism</option>
                                                                {{--<option value="ChamberofAgricultureandlivestock">Chamber of Agriculture and livestock</option>--}}
                                                                {{--<option value="Chamber of Arts Crafts and Artisans">Chamber of Arts Crafts and Artisans</option>--}}
                                                                {{--<option value="Chamber of Commerce and Services">Chamber of Commerce and Services</option>--}}
                                                                {{--<option value="Chamber of Financial Institutions">Chamber of Financial Institutions</option>--}}
                                                                {{--<option value="Chamber of ICT">Chamber of ICT</option>--}}
                                                                {{--<option value="Chamber of Industry">Chamber of Industry</option>--}}
                                                                {{--<option value="Chamber of Liberal Profession">Chamber of Liberal Profession</option>--}}

                                                                {{--<option value="Chamber of Women Entrepreneurs">Chamber of Women Entrepreneurs</option>--}}
                                                                {{--<option value="Chamber of Young Entrepreneurs">Chamber of Young Entrepreneurs</option>--}}


                                                            </select>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">First name </label>
                                                            <input type="text" class="input-text" name="firstname" id="billing_last_name" placeholder="" value="{{ old('firstname') }}" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Last name </label>
                                                            <input type="text" class="input-text" name="lastname" id="billing_last_name" placeholder="" value="{{ old('lastname') }}" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">TIN number</label>
                                                            <input type="text" class="input-text" name="tinnumber" id="billing_last_name" placeholder="" value="{{ old('tinnumber') }}"/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Telephone number</label>
                                                            <input  id="billing_last_name" class="input-text"
                                                                    name="telephonenumber" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                    type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Email</label>
                                                            <input type="text" class="input-text" name="email" id="billing_last_name" placeholder="" value="{{ old('email') }}" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Website *optional* </label>
                                                            <input type="text" class="input-text" name="website" id="billing_last_name" placeholder="" value="{{ old('website') }}" />
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Date of Birth</label>
                                                            <input type="date" class="input-text" name="age" id="billing_last_name" placeholder="year-month-date" value="{{ old('age') }}" required/>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Nationality</label>
                                                            <input type="text" class="input-text" name="nationality" id="billing_last_name" placeholder="" value="{{ old('nationality') }}" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Province</label>
                                                            <select class="" id="billing_last_name" name="province"  required>
                                                                <option value="{{ old('province') }}">{{ old('province') }}</option>
                                                                <option value="City of Kigali">City of Kigali</option>
                                                                <option value="Eastern Province">Eastern Province</option>
                                                                <option value="Western Province">Western Province</option>
                                                                <option value="Southern Province">Southern Province</option>
                                                                <option value="Northern Province">Northern Province</option>
                                                            </select>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">District</label>
                                                            <select class="" id="billing_last_name" name="district" required>
                                                                <option value="{{ old('district') }}">{{ old('district') }}</option>
                                                                <option value="Bugesera">Bugesera</option>
                                                                <option value="Burera">Burera</option>
                                                                <option value="Gakenke">Gakenke</option>
                                                                <option value="Gasaba">Gasabo</option>
                                                                <option value="Gatsibo">Gatsibo</option>
                                                                <option value="Gicumbi">Gicumbi</option>
                                                                <option value="Gisagara">Gisagara</option>
                                                                <option value="Huye">Huye</option>
                                                                <option value="Kamonyi">Kamonyi</option>
                                                                <option value="Karongi">Karongi</option>
                                                                <option value="Kayonza">Kayonza</option>
                                                                <option value="Kicukiro">Kicukiro</option>
                                                                <option value="Kirehe">Kirehe</option>
                                                                <option value="Muhanga">Muhanga</option>
                                                                <option value="Musanze">Musanze</option>
                                                                <option value="Ngoma">Ngoma</option>
                                                                <option value="Ngororero">Ngororero</option>
                                                                <option value="Nyabihu">Nyabihu</option>
                                                                <option value="Nyagatare">Nyagatare</option>
                                                                <option value="Nyamagabe">Nyamagabe</option>
                                                                <option value="Nyamasheke">Nyamasheke</option>
                                                                <option value="Nyanza">Nyanza</option>
                                                                <option value="Nyarugenge">Nyarugenge</option>
                                                                <option value="Nyaruguru">Nyaruguru</option>
                                                                <option value="Rubavu">Rubavu</option>
                                                                <option value="Ruhango">Ruhango</option>
                                                                <option value="Rulindo">Rulindo</option>
                                                                <option value="Rusuzi">Rusizi</option>
                                                                <option value="Rutsiro">Rutsiro</option>
                                                                <option value="Rwamagana">Rwamagana</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Sector</label>
                                                            <input type="text" class="input-text" name="sector" id="billing_last_name" placeholder="" value="{{ old('sector') }}" required/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Cell</label>
                                                            <input type="text" class="input-text" name="cell" id="billing_last_name" placeholder="" value="{{ old('cell') }}" required/>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Working experience in tourism (Years)</label>
                                                            <select class="" id="billing_last_name" name="workingexperience"  required>
                                                                <option value="{{ old('workingexperience') }}">{{ old('workingexperience') }}</option>
                                                                <option value="from 0 to 1">from 0 to 1</option>
                                                                <option value="from 1 to 5">from 1 to 5</option>
                                                                <option value="from 6 to 10">from 6 to 10</option>
                                                                <option value="from 11 to 20">from 11 to 20</option>
                                                                <option value="from 21 to 30">from 21 to 30</option>
                                                                <option value="from 31 to 40">from 31 to 40</option>
                                                                <option value="from 41 to 50">from 41 to 50</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Education level</label>

                                                            <select class="" id="billing_last_name" name="educationlevel"  required>
                                                                <option value="{{ old('educationlevel') }}">{{ old('educationlevel') }}</option>
                                                                <option value="Middle School/ O'level certificate">Middle School/ O'level certificate</option>
                                                                <option value="High school/College certificate">High school/College certificate</option>
                                                                <option value="Bachelor's degree">Bachelor's degree</option>
                                                                <option value="Master's degree">Master's degree</option>

                                                            </select>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Your specialization as a tour guide</label>

                                                            <select class="billing_country country_to_state country_select " name="areofinterest"  required>
                                                                <option value="{{ old('areofinterest') }}">{{ old('areofinterest') }}</option>
                                                                <option value="bird Cultural/heritage">Cultural,heritage and community tours</option>
                                                                <option value="wildlife tours">wildlife tours</option>
                                                                <option value="savannah tours">savannah game drive</option>
                                                                <option value="primate tours">primate tours</option>
                                                                <option value="walking tours">walking tours</option>
                                                                <option value="city tours">city tours</option>
                                                                <option value="Hiking trekking tours">Hiking trekking tours</option>
                                                                <option value="religious tours ">religious tours </option>
                                                                <option value="cycling tours">cycling tours</option>
                                                                <option value="boat trips">boat trips</option>
                                                                <option value="kayaking tours">kayaking tours</option>
                                                                <option value="fishing tours">fishing tours</option>
                                                                <option value="canoeing tours">canoeing tours</option>
                                                                <option value="Tea & Coffee tours">Tea & Coffee tours</option>
                                                            </select>
                                                            <!--                                                            <input type="text" class="input-text" name="areofinterest" id="billing_last_name" placeholder="" value="" />-->
                                                        </p>
                                                        {{--<p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">--}}
                                                            {{--<label for="billing_last_name" class="">Export/Import Goods/Services</label>--}}
                                                            {{--<select class="billing_country country_to_state country_select " name="export"  >--}}
                                                                {{--<option value="{{ old('export') }}">{{ old('export') }}</option>--}}
                                                                {{--<option value="beverage">beverage</option>--}}
                                                                {{--<option value="coffee">coffee</option>--}}
                                                                {{--<option value="cars">cars</option>--}}
                                                                {{--<option value="electronic equipment">electronic equipment</option>--}}
                                                                {{--<option value="food products">food products</option>--}}
                                                                {{--<option value="gas">gas</option>--}}
                                                                {{--<option value="hardware">hardware</option>--}}
                                                                {{--<option value="horticulture">horticulture</option>--}}
                                                                {{--<option value="oil">oil</option>--}}
                                                                {{--<option value="pharmaceutical products">pharmaceutical products</option>--}}
                                                                {{--<option value="services">services</option>--}}
                                                                {{--<option value="spare parts">spare parts</option>--}}
                                                                {{--<option value="stationary">stationary</option>--}}
                                                                {{--<option value="Tea">Tea</option>--}}
                                                            {{--</select>--}}
                                                        {{--</p>--}}


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Attach your passport photo</label>
                                                            <input type="file" class="input-text" name="attachyourpassport" id="billing_last_name" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Attach your CV (only PDF is accepted)</label>
                                                            <input type="file" class="input-text" name="attachyourcv" id="billing_last_name"  required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Attach your diplomas,certificates (only PDF is accepted)</label>
                                                            <input type="file" class="input-text" name="attachyourdiplomas" id="billing_last_name" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">National ID or Passport or Driving license (only PDF is accepted)</label>
                                                            <input type="file" class="input-text" name="drivinglicense" id="billing_last_name" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Criminal record certificate (only PDF is accepted)</label>
                                                            <input type="file" class="input-text" name="criminal_record" id="criminal_record" required/>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Description (No more than 500 characters) </label>
                                                            <textarea rows="10" class="input-text" name="description" id="description" maxlength="500" required>{{ old('description') }}</textarea>
                                                        </p>

                                                        <p class="form-row form-row notes"><input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit"  style="margin: 0px !important;"/></p>
                                                    </div>

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="divider-wrap col-sm-12">
                                        <div class="spb_divider thin spb_content_element " style="margin-top: 30px; margin-bottom: 60px;"></div>
                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>

    @include('layouts.footer')
@endsection