@extends('layouts.master')

@section('title', 'RSGA')

@section('content')

    @include('layouts.topmenu')


    <div id="sf-mobile-slideout-backdrop"></div>
<div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/slider3.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style=""></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">CONTACT US</h1>
            </div>
        </div>
    </div>
    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-13072 page type-page status-publish hentry" id="13072">
                        <section class="row fw-row ">
                            <div class="spb_gmaps_widget fullscreen-map spb_content_element col-sm-12">
                                <div class="spb-asset-content">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d31899.99867908748!2d30.103524999999998!3d-1.9533690000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc76b5e00aaa31b51!2sM%26M+Plaza!5e0!3m2!1sen!2srw!4v1531143935903" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </section>
                        <section class="container ">
                            <div class="row">
                                <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                            </div>
                        </section>
                        <section class="container ">
                            <div class="row">
                                <div class="spb_content_element col-sm-6 spb_text_column">
                                    <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                        <div role="form" class="wpcf7" id="wpcf7-f15387-p13072-o1" lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <form action="/pages/contact/#wpcf7-f15387-p13072-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                <div style="display: none;">
                                                    <input type="hidden" name="_wpcf7" value="15387" />
                                                    <input type="hidden" name="_wpcf7_version" value="4.4.2" />
                                                    <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f15387-p13072-o1" />
                                                    <input type="hidden" name="_wpnonce" value="7ae6c8aa2d" />
                                                </div>
                                                <p><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Name" /></span><br />
                                                    <span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-invalid="false" placeholder="Address" /></span><br />
                                                    <span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Subject" /></span><br />
                                                    <span class="wpcf7-form-control-wrap message"><textarea name="message" cols="40" rows="5" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span><br />
                                                    <input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" />
                                                </p>
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="spb-column-container col-sm-4   " style="padding-left:15px; padding-right:15px; ">
                                    <div class="spb-asset-content" style="">
                                        <section class="container "><div class="row">
                                                <div class="spb_content_element col-sm-12 spb_text_column">
                                                    <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                        <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Office Contacts</span></h3></div>
                                                        <p>Call: +250 788304524<br />
                                                            Email: <a href="" class="__cf_email__" style="color: #000 !important;">info@rsga.rw</a> or <br>
                                                            <a href="" class="__cf_email__" style="color: #000 !important;">rwasagainfo@gmail.com</a></p>
                                                    </div>
                                                </div> </div></section>
                                        <section class="container "><div class="row"><div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section class="container "><div class="row">
                                                <div class="spb_content_element col-sm-12 spb_text_column">
                                                    <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                        <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Follow us</span></h3></div>
                                                        <ul class="social-icons standard ">
                                                            <li class="twitter"><a href="" target="_blank"><i class="fa-twitter"></i><i class="fa-twitter"></i></a></li>
                                                            <li class="facebook"><a href="" target="_blank"><i class="fa-facebook"></i><i class="fa-facebook"></i></a></li>
                                                            <li class="instagram"><a href="" target="_blank"><i class="fa-instagram"></i><i class="fa-instagram"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div> </div>
                                        </section>
                                    </div>
                                </div>
                                <div class="spb-column-container col-sm-2   " style="padding-left:15px; padding-right:15px; ">
                                    <div class="spb-asset-content" style="">
                                        <section class="container "><div class="row">
                                                <div class="spb_content_element col-sm-12 spb_text_column">
                                                    <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                        <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Our Address</span></h3></div>
                                                        <p>Kigali,Rwanda<br />
                                                            Gishushu,<br />
                                                            M&M Plaza <br />
                                                            5Th Floor</p>
                                                    </div>
                                                </div> </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
@include('layouts.footer')
@endsection