@extends('layouts.master')

@section('title', 'RHA')

@section('content')

    @include('layouts.topmenu')
    <style>
        p{
            text-align: justify !important;
        }
        #header .std-menu ul.mega-menu>li>a {
            padding: 0 10px;
            text-transform: uppercase;
            text-decoration: none !important;
        }
        .title-wrap .spb-heading>span:hover {
            display: inline-block;
            border: 1px solid #6c4428;
            /* color: #fff; */
            padding: 10px;
        }
    </style>

    <?php
    use Carbon\Carbon;
    ?>
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/ubumwe.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style=""></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">News</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width col-sm-12 hidden-xs col-natural" data-row-style="" data-v-center="false" data-top-style="none" data-bottom-style="none" style="margin-top: 0%!important;margin-left: 0%!important;margin-right: 0%!important;margin-bottom: 0%!important;border-top: 0px default !important;border-left: 0px default !important;border-right: 0px default !important;border-bottom: 0px default !important;padding-top: 0%!important;padding-left: 0%!important;padding-right: 0%!important;padding-bottom: 0%!important;background-color:undefined!important;">
                                    <div class="spb_content_element" style="">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            @foreach($newpost as $datas)
                                                <div class="row" style="border-bottom: solid 2px #414141; margin-bottom: 10px !important;">
                                                    <div class="col-md-8" style="padding: 0px">
                                                        <div class="title-wrap">
                                                            <h3 class="spb-heading spb-text-heading" style="color: #6c4428 !important;"><span>{{$datas->news_title}}</span></h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" style="padding: 0px">
                                                        <div class="title-wrap">
                                                            <h3 class="spb-heading spb-text-heading" style="color: #000 !important;float: right;font-size: 11px !important;">
                                                        <span>Date:
                                                            <?php
                                                            $date = $datas->created_at;
                                                            $dates = $date->toDateString();
                                                            $datesformat = new DateTime($dates);
                                                            echo $datesformat->format('d-m-Y');
                                                            ?>
                                                            {{--{{$datas->created_at}}--}}
                                                        </span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="padding: 0px">
                                                        <p>
                                                            <?php
                                                            $str = $datas->news_details;

                                                            echo $str
                                                            ?>
                                                        </p>

                                                    </div>




                                                    {{--<p><STRONG></STRONG></p>--}}

                                                </div>
                                            @endforeach
                                        </section>

                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
    @include('layouts.footer')
@endsection