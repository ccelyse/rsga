@extends('layouts.master')

@section('title', 'RSGA')

@section('content')

    @include('layouts.topmenu')
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/lakekivu3.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style=""></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">LICENSING</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width col-sm-12 hidden-xs col-natural" data-row-style="" data-v-center="false" data-top-style="none" data-bottom-style="none" style="margin-top: 0%!important;margin-left: 0%!important;margin-right: 0%!important;margin-bottom: 0%!important;border-top: 0px default !important;border-left: 0px default !important;border-right: 0px default !important;border-bottom: 0px default !important;padding-top: 0%!important;padding-left: 0%!important;padding-right: 0%!important;padding-bottom: 0%!important;background-color:undefined!important;">
                                    <div class="spb_content_element" style="">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                                <p>In 2016, the Rwanda Development Board (RDB) officially introduced the tourism operating license to support the regulation of tourism entities/operators.</p>
                                                <p>The law regulating the tourism industry in Rwanda officially came into force on 28 July 2014, while ministerial orders regulating tourism business operating licenses and the grading of tourism entities came into effect on 3 October 2016. Both the law and ministerial orders were developed in accordance with East African Community (EAC) standards.</p>
                                                <p>This industry regulation is aimed at improving quality of service, build the capacity of the private sector, and improve the marketing of Rwanda as a tourism destination.</p>

                                                <div class="title-wrap">
                                                    <h3 class="spb-heading spb-text-heading" style="color: #000 !important;"><span>Who can apply for a tourism operating license?</span></h3>
                                                </div>
                                                <p>Owners or operators of the entities in the below categories can apply for a tourism operating license:</p>
                                                <ul class="sf-list ">
                                                    <li><i class="sf-icon-right-chevron"></i><span>A: Accommodation establishments (town hotels, vacation hotels, motels, lodges, tented camps, serviced apartments, cottages and villas)</span></li>
                                                    <li><i class="sf-icon-right-chevron"></i><span> B: Restaurants, bars and nightclubs</span></li>
                                                    <li><i class="sf-icon-right-chevron"></i><span>C: Tour operators, tour guides and travel agents</span></li>
                                                    <li><i class="sf-icon-right-chevron"></i><span>D: Tourism information centers</span></li>
                                                    <li><i class="sf-icon-right-chevron"></i><span>E: Museums and cultural tourism</span></li>

                                                </ul>
                                                <p><strong>NOTE:</strong>It is a requirement to hold a valid membership certificate of RSGA to apply for your operating license.</p>
                                                <p>Click on below link to access the full checklist of the required documents to apply for your operating license.</p>

                                                <ul class="sf-list ">
                                                    <li><i class="sf-icon-right-chevron"></i><span><a href="https://tourismregulation.rw/static/docs/uploads/document_checklists/Tour%20Guide%20-%20Document%20checklist.pdf" style="color:#006937 !important;font-weight: bold;" target="_blank">Documents checklist</a> </span></li>

                                                </ul>

                                                <p>The operating license is applied via <a href="www.tourismregulation.rw" style="color: #006937 !important;">www.tourismregulation.rw.</a> The website serves as a portal for tourism entities/operators to access tourism regulation information and services. In addition, it gives the operators access to more information regarding the regulation.</p>
                                                <p>RSGA team remains at your disposal to offer any necessary assistance in order to comply with the regulation.</p>



                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>

    @include('layouts.footer')
@endsection