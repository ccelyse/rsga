@extends('layouts.master')

@section('title', 'RSGA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
    </style>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/16195632_1328142503874779_7653420126536604310_n.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0.5;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">About RSGA</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>A brief history about RSGA</span></h3>
                                            </div>
                                            <p>Rwanda Safari Guides Association (RSGA) is one of the five associations within the Chamber of Tourism  which is one of the ten Chambers of the Private Sector Federation, the umbrella organisation. Currently RSGA has over 150 registered members countrywide. </p>
                                            <p>RSGA carries out a regular grading and classification exercise of guides in a bid to determine the standards and assure of predictable levels of quality of service, professionalism and the ability of a guide to offer the experience that meets tourists’ expectations.</p>
                                            <p>This complements the national licensing program by the Rwanda Development Board and helps to inform tourists on how to recognize quality and service excellence and to know that the guides offer services at a nationally recognized standard for greater transparency and value for money.</p>
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Mission</span></h3>
                                            </div>
                                            <p>To provide our to clients with professional guiding services based on sound knowledge, integrity and passion for a safe and memorable experience.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Vision</span></h3>
                                            </div>
                                            <p>To develop a culture of professional guiding through the provision of training, networking, mentorship and grading programs to our members.</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                    <div class="spb_content_element" style="background-image: url(images/NY4.jpg);background-position: center;background-size: cover;">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                                <div class="team_list carousel-asset spb_content_element col-sm-12">
                                                    <div class="spb-asset-content">
                                                        <div class="title-wrap clearfix">
                                                            <h4 class="spb-heading" style="color: #fff !important;"><span>Meet Our Executive Committee</span></h4>
                                                            <div class="carousel-arrows"><a href="index.php#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.php#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a></div>
                                                        </div>
                                                        <div class="team-carousel carousel-wrap">
                                                            <div id="carousel-1" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="5" data-auto="false">
                                                                <div itemscope data-id="id-0" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="212"></a><img itemprop="image" src="committee/eric kayiranga.jpg" width="270" height="270" alt="ERIC KAYIRANGA" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="212">ERIC KAYIRANGA</a></h4>
                                                                            <h5 class="team-member-position">Chairman</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
{{--                                                                <div itemscope data-id="id-1" class="clearfix team-member carousel-item">--}}
{{--                                                                    <div class="team-member-item-wrap">--}}
{{--                                                                        <figure class="animated-overlay">--}}
{{--                                                                            <a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="GASIGWA Omar" />--}}
{{--                                                                            <figcaption class="team-standard-alt">--}}
{{--                                                                                <div class="thumb-info thumb-info-alt">--}}
{{--                                                                                    <i>--}}
{{--                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">--}}
{{--                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />--}}
{{--                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />--}}
{{--                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />--}}
{{--                                                               </svg>--}}
{{--                                                                                    </i>--}}
{{--                                                                                </div>--}}
{{--                                                                            </figcaption>--}}
{{--                                                                        </figure>--}}
{{--                                                                        <div class="team-member-details-wrap">--}}
{{--                                                                            <h4 class="team-member-name"><a href="" class="" data-id="213">GASIGWA Omar</a></h4>--}}
{{--                                                                            <h5 class="team-member-position">1St Vice Chairman</h5>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                                <div itemscope data-id="id-2" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="214"></a><img itemprop="image" src="committee/MARTIN MUYENZI.jpeg" width="270" height="270" alt="Martin Muyenzi" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="214">Martin Muyenzi </a></h4>
                                                                            <h5 class="team-member-position">2nd Vice Chairman</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-3" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="215"></a><img itemprop="image" src="committee/INGRID.jpeg" width="270" height="270" alt="Ingrid Jeanne Elyse Ishimirwe" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="215">Ingrid Jeanne Elyse Ishimirwe</a></h4>
                                                                            <h5 class="team-member-position">Secretary</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-4" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="222"></a><img itemprop="image" src="committee/ERIC MANZI.jpeg" width="270" height="270" alt="MANZI Eric" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="222">MANZI Eric</a></h4>
                                                                            <h5 class="team-member-position">Treasurer</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-1" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="committee/YASSIN HAMDAN.jpeg" width="270" height="270" alt="Yasin Hamdan Hakizimana" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="213">Yasin Hamdan Hakizimana</a></h4>
                                                                            <h5 class="team-member-position">Commmissioner for Information and Public Relations</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="team_list carousel-asset spb_content_element col-sm-12">
                                                    <div class="spb-asset-content">
                                                        <div class="title-wrap clearfix">
                                                            <h4 class="spb-heading" style="color: #fff !important;"><span>ADVISORS</span></h4>
                                                            <div class="carousel-arrows"><a href="index.php#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.php#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a></div>
                                                        </div>
                                                        <div class="team-carousel carousel-wrap">
                                                            <div id="carousel-2" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="4" data-auto="false">
                                                                <div itemscope data-id="id-0" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="212"></a><img itemprop="image" src="committee/MUHIRE JEREMIAH.jpg" width="270" height="270" alt="MUHIRE JEREMIAH" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="212">Jeremiah Muhire</a></h4>
                                                                            <h5 class="team-member-position">ADVISOR (In charge of Capacity building)</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-1" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="committee/kirenga.jpg" width="270" height="270" alt="Kamugisha Kirenga" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="213">Kamugisha Kirenga</a></h4>
                                                                            <h5 class="team-member-position">ADVISOR (In charge of East african relations)</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="team_list carousel-asset spb_content_element col-sm-12">
                                                    <div class="spb-asset-content">
                                                        <div class="title-wrap clearfix">
                                                            <h4 class="spb-heading" style="color: #fff !important;"><span>DISPLINARY Committee</span></h4>
                                                            <div class="carousel-arrows"><a href="index.php#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.php#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a></div>
                                                        </div>
                                                        <div class="team-carousel carousel-wrap">
                                                            <div id="carousel-3" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="5" data-auto="false">
{{--                                                                <div itemscope data-id="id-0" class="clearfix team-member carousel-item">--}}
{{--                                                                    <div class="team-member-item-wrap">--}}
{{--                                                                        <figure class="animated-overlay">--}}
{{--                                                                            <a class="team-member-link " href="" data-id="212"></a><img itemprop="image" src="" width="270" height="270" alt="Busingye Jackline" />--}}
{{--                                                                            <figcaption class="team-standard-alt">--}}
{{--                                                                                <div class="thumb-info thumb-info-alt">--}}
{{--                                                                                    <i>--}}
{{--                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">--}}
{{--                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />--}}
{{--                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />--}}
{{--                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />--}}
{{--                                                               </svg>--}}
{{--                                                                                    </i>--}}
{{--                                                                                </div>--}}
{{--                                                                            </figcaption>--}}
{{--                                                                        </figure>--}}
{{--                                                                        <div class="team-member-details-wrap">--}}
{{--                                                                            <h4 class="team-member-name"><a href="" class="" data-id="212">Busingye Jackline</a></h4>--}}
{{--                                                                            <h5 class="team-member-position">Chairperson</h5>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                                <div itemscope data-id="id-1" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="committee/MAGEZI JEAN SAUVEUR.jpeg" width="270" height="270" alt="Magezi Jean Sauveur" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="213">Magezi Jean Sauveur</a></h4>
                                                                            <h5 class="team-member-position">First Vice</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
{{--                                                                <div itemscope data-id="id-2" class="clearfix team-member carousel-item">--}}
{{--                                                                    <div class="team-member-item-wrap">--}}
{{--                                                                        <figure class="animated-overlay">--}}
{{--                                                                            <a class="team-member-link " href="" data-id="214"></a><img itemprop="image" src="" width="270" height="270" alt="Mukwaya Robert" />--}}
{{--                                                                            <figcaption class="team-standard-alt">--}}
{{--                                                                                <div class="thumb-info thumb-info-alt">--}}
{{--                                                                                    <i>--}}
{{--                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">--}}
{{--                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />--}}
{{--                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />--}}
{{--                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />--}}
{{--                                                               </svg>--}}
{{--                                                                                    </i>--}}
{{--                                                                                </div>--}}
{{--                                                                            </figcaption>--}}
{{--                                                                        </figure>--}}
{{--                                                                        <div class="team-member-details-wrap">--}}
{{--                                                                            <h4 class="team-member-name"><a href="" class="" data-id="214">Mukwaya Robert</a></h4>--}}
{{--                                                                            <h5 class="team-member-position">2nd Vice</h5>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection