@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
    </style>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif

                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditSubCompany_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($editsub as $datas)
                                            <div class="form-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Company Category</label>
                                                            <input type="text" name="id" value="{{$datas->id}}" hidden>
                                                            <select class="form-control" id="basicSelect" name="companycategory">
                                                                <option value="{{$datas->companycategory}}">{{$datas->companycategory}}</option>
                                                                @foreach($listcomp as $data)
                                                                    <option value="{{$data->company_category}}">{{$data->company_category}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Sub Company Category</label>
                                                            <select class="form-control" id="basicSelect" name="subcompanycategory">
                                                                <option value="{{$datas->subcompanycategory}}">{{$datas->subcompanycategory}}</option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                                                                {{--<option value="Hotel">Hotel</option>--}}
                                                                {{--<option value="Apartment">Apartment</option>--}}
                                                                {{--<option value="Villa">Villa</option>--}}
                                                                {{--<option value="Motel">Motel</option>--}}
                                                                {{--<option value="Cottage">Cottage</option>--}}
                                                                {{--<option value="Logde">Logde</option>--}}
                                                                {{--<option value="Tented camp">Tented camp</option>--}}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Seating Capacity</label>
                                                            <select class="form-control" id="basicSelect" name="seatingcapacity">
                                                                <option value="{{$datas->seatingcapacity}}">{{$datas->seatingcapacity}}</option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Number of Room</label>
                                                            <select class="form-control" id="basicSelect" name="numberofroom">
                                                                <option value="{{$datas->numberofroom}}">{{$datas->numberofroom}}</option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Grade</label>
                                                            <select class="form-control" id="basicSelect" name="Grade">
                                                                <option value="{{$datas->Grade}}">{{$datas->Grade}}</option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>
                                            @endforeach
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
