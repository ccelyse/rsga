@extends('backend.layout.master')

@section('title', 'RSGA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #006937 !important;
            border-color:#006937 !important;
        }
        .btn-primary:hover{
            background-color: #006937 !important;
            border-color:#006937 !important;
        }
    </style>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Member info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditJoinMember_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($edit as $data)
                                            <div class="form-body">
                                                <div class="row">
                                                    <input type="text" id="projectinput1" class="form-control" value="{{ $data->id }}"
                                                           name="id" required hidden>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Type of Membership</label>
                                                            <select class="form-control"  id="basicSelect" name="typeofmembership">
                                                                <option value="{{$data->typeofmembership}}">{{$data->typeofmembership}}</option>
                                                                <option value="Association" selected>Association</option>
                                                                <option value="Champions (Indashyikirwa)">Champions (Indashyikirwa)</option>
                                                                <option value="District">District</option>
                                                                <option value="Golden Circle">Golden Circle</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Chamber</label>
                                                            <select class="form-control"  id="basicSelect" name="chamber">
                                                                <option value="{{$data->chamber}}">{{$data->chamber}}</option>
                                                                <option value="Chamber of Tourism" selected>Chamber of Tourism</option>
                                                                {{--<option value="ChamberofAgricultureandlivestock">Chamber of Agriculture and livestock</option>--}}
                                                                {{--<option value="Chamber of Arts Crafts and Artisans">Chamber of Arts Crafts and Artisans</option>--}}
                                                                {{--<option value="Chamber of Commerce and Services">Chamber of Commerce and Services</option>--}}
                                                                {{--<option value="Chamber of Financial Institutions">Chamber of Financial Institutions</option>--}}
                                                                {{--<option value="Chamber of ICT">Chamber of ICT</option>--}}
                                                                {{--<option value="Chamber of Industry">Chamber of Industry</option>--}}
                                                                {{--<option value="Chamber of Liberal Profession">Chamber of Liberal Profession</option>--}}

                                                                {{--<option value="Chamber of Women Entrepreneurs">Chamber of Women Entrepreneurs</option>--}}
                                                                {{--<option value="Chamber of Young Entrepreneurs">Chamber of Young Entrepreneurs</option>--}}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">First Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->firstname }}"
                                                                   name="firstname">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Last Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->lastname }}"
                                                                   name="lastname">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Tin number</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->tinnumber }}" name="tinnumber">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Phone number</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->telephonenumber }}"
                                                                   name="telephonenumber">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Email Address</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->email }}"
                                                                   name="email">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Website</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->website }}"
                                                                   name="website">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Date of Birth</label>
                                                            <input type="date" id="projectinput1" class="form-control" value="{{ $data->age }}"
                                                                   name="age">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Nationality</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->nationality }}"
                                                                   name="nationality">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Province</label>
                                                            <select class="form-control"  id="basicSelect" name="province">
                                                                <option value="{{$data->province}}">{{$data->province}}</option>
                                                                <option value="City of Kigali">City of Kigali</option>
                                                                <option value="Eastern Province">Eastern Province</option>
                                                                <option value="Western Province">Western Province</option>
                                                                <option value="Southern Province">Southern Province</option>
                                                                <option value="Northern Province">Northern Province</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">District</label>
                                                            <select class="form-control"  id="basicSelect" name="district">
                                                                <option value="{{$data->district}}">{{$data->district}}</option>
                                                                <option value="Bugesera">Bugesera</option>
                                                                <option value="Burera">Burera</option>
                                                                <option value="Gakenke">Gakenke</option>
                                                                <option value="Gasabo">Gasabo</option>
                                                                <option value="Gatsibo">Gatsibo</option>
                                                                <option value="Gicumbi">Gicumbi</option>
                                                                <option value="Gisagara">Gisagara</option>
                                                                <option value="Huye">Huye</option>
                                                                <option value="Kamonyi">Kamonyi</option>
                                                                <option value="Karongi">Karongi</option>
                                                                <option value="Kayonza">Kayonza</option>
                                                                <option value="Kicukiro">Kicukiro</option>
                                                                <option value="Kirehe">Kirehe</option>
                                                                <option value="Muhanga">Muhanga</option>
                                                                <option value="Musanze">Musanze</option>
                                                                <option value="Ngoma">Ngoma</option>
                                                                <option value="Ngororero">Ngororero</option>
                                                                <option value="Nyabihu">Nyabihu</option>
                                                                <option value="Nyagatare">Nyagatare</option>
                                                                <option value="Nyamagabe">Nyamagabe</option>
                                                                <option value="Nyamasheke">Nyamasheke</option>
                                                                <option value="Nyanza">Nyanza</option>
                                                                <option value="Nyarugenge">Nyarugenge</option>
                                                                <option value="Nyaruguru">Nyaruguru</option>
                                                                <option value="Rubavu">Rubavu</option>
                                                                <option value="Ruhango">Ruhango</option>
                                                                <option value="Rulindo">Rulindo</option>
                                                                <option value="Rusuzi">Rusuzi</option>
                                                                <option value="Rutsiro">Rutsiro</option>
                                                                <option value="Rwamagana">Rwamagana</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Sector</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->sector }}"
                                                                   name="sector">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Cell</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->cell }}"
                                                                   name="cell">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Working experience in tourism (Years)</label>
                                                            <select class="form-control"  id="basicSelect" name="workingexperience">
                                                                <option value="{{$data->workingexperience}}">{{$data->workingexperience}}</option>
                                                                <option value="from 0 to 1">from 0 to 1</option>
                                                                <option value="from 1 to 5">from 1 to 5</option>
                                                                <option value="from 6 to 10">from 6 to 10</option>
                                                                <option value="from 11 to 20">from 11 to 20</option>
                                                                <option value="from 21 to 30">from 21 to 30</option>
                                                                <option value="from 31 to 40">from 31 to 40</option>
                                                                <option value="from 41 to 50">from 41 to 50</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Education level</label>
                                                            <select class="form-control"  id="basicSelect" name="educationlevel" required>
                                                                <option value="{{$data->educationlevel}}">{{$data->educationlevel}}</option>
                                                                <option value="Middle School/ O'level certificate">Middle School/ O'level certificate</option>
                                                                <option value="High school/College certificate">High school/College certificate</option>
                                                                <option value="Bachelor's degree">Bachelor's degree</option>
                                                                <option value="Master's degree">Master's degree</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Your specialization as a tour guide</label>
                                                            <select class="form-control"  id="basicSelect" name="areofinterest">
                                                                <option value="{{$data->areofinterest}}">{{$data->areofinterest}}</option>
                                                                <option value="bird Cultural/heritage">Cultural,heritage and community tours</option>
                                                                <option value="wildlife tours">wildlife tours</option>
                                                                <option value="savannah tours">savannah game drive</option>
                                                                <option value="primate tours">primate tours</option>
                                                                <option value="walking tours">walking tours</option>
                                                                <option value="city tours">city tours</option>
                                                                <option value="Hiking trekking tours">Hiking trekking tours</option>
                                                                <option value="religious tours ">religious tours </option>
                                                                <option value="cycling tours">cycling tours</option>
                                                                <option value="boat trips">boat trips</option>
                                                                <option value="kayaking tours">kayaking tours</option>
                                                                <option value="fishing tours">fishing tours</option>
                                                                <option value="canoeing tours">canoeing tours</option>
                                                                <option value="Tea & Coffee tours">Tea & Coffee tours</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Attach your passport photo</label>--}}
                                                            {{--<input type="file" name="attachyourpassport">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Attach your CV (only PDF is accepted)</label>--}}
                                                            {{--<input type="file" name="attachyourcv">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Attach your diplomas,certificates (only PDF is accepted)</label>--}}
                                                            {{--<input type="file" name="attachyourdiplomas">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">National ID or Passport or Driving license (only PDF is accepted)</label>--}}
                                                            {{--<input type="file" name="drivinglicense">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label for="projectinput1">Criminal record certificate (only PDF is accepted)</label>--}}
                                                            {{--<input type="file" name="criminal_record">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Description</label>
                                                            <textarea rows="10" id="projectinput1" class="form-control" name="description">{{ $data->description }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" hidden>
                                                        <div class="form-group">
                                                            <label for="projectinput1">Sector</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->Memberstatus }}"
                                                                   name="Memberstatus">
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>
                                            @endforeach

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
