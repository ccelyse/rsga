@extends('backend.layout.master')

@section('title', 'RSGA')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
<style>
    .btn-secondary{
        color:#fff !important;
        background-color: #006935 !important;
        border-color: #006935 !important;
    }
    #ui-datepicker-div{
        padding: 10px;
        table-responsive;
        background:#6b442b;
    }
    .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
        color: #fff !important;
        padding: 10px;
    }
</style>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body"><!-- HTML5 export buttons table -->
                <section id="html5">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">RSGA Members List</h4>
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    @if (session('update'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('update') }}
                                        </div>
                                    @endif
                                    @if (session('danger'))
                                        <div class="alert alert-danger" id="success_messages" style="margin-top: 10px;">
                                            {{ session('danger') }}
                                        </div>
                                    @endif

                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Notify Member</th>
                                                <th>Type of Membership</th>
                                                <th>Chamber</th>
                                                <th>First name</th>
                                                <th>Last name</th>
                                                <th>TIN number</th>
                                                <th>Telephone number</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Date of Birth</th>
                                                <th>Nationality</th>
                                                <th>Province</th>
                                                <th>District</th>
                                                <th>Sector</th>
                                                <th>Cell</th>

                                                <th>Working experience in tourism (Years)</th>
                                                <th>Education level</th>
                                                <th>Your specialization as a tour guide</th>
                                                <th>Export/Import Goods/Services</th>
                                                <th>Passport Photo</th>
                                                <th>CV</th>
                                                <th>Ediplomas,certificates</th>
                                                <th>Driving License</th>

                                                <th>Approval</th>
                                                <th>Approved Date</th>
                                                <th>Expiration Date</th>
                                                <th>Identification</th>
                                                <th>Date Created</th>
                                                {{--<th>Add Bank Slip</th>--}}
                                                <th>Add Bank Slip</th>
                                                <th>Last Edit</th>
                                                {{--<th>User</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listmembers as $data)
                                                <tr>
                                                    <td>
                                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#notify{{$data->id}}">
                                                            Notify Member
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="notify{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1"> Notify Member</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('NotifyMember') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}

                                                                            <div class="row  multi-field">
                                                                                <div class='col-md-12'>
                                                                                    {{--<div class='form-group'>--}}
                                                                                        {{--<label for='projectinput1'>Message</label>--}}
                                                                                        {{--<textarea name='rsgamessage'--}}
                                                                                                  {{--rows='5'--}}
                                                                                                  {{--class='form-control'></textarea>--}}
                                                                                    {{--</div>--}}
                                                                                    <script language="javascript" type="text/javascript">
                                                                                        function limitText(limitField, limitCount, limitNum) {
                                                                                            if (limitField.value.length > limitNum) {
                                                                                                limitField.value = limitField.value.substring(0, limitNum);
                                                                                            } else {
                                                                                                limitCount.value = limitNum - limitField.value.length;
                                                                                            }
                                                                                        }
                                                                                    </script>

                                                                                    <textarea class='form-control' name="limitedtextarea" onKeyDown="limitText(this.form.limitedtextarea,this.form.countdown,160);"
                                                                                              onKeyUp="limitText(this.form.limitedtextarea,this.form.countdown,160);" rows="5" required>Hi, {{$data->firstname}} {{$data->lastname}}, Here is your backend credentials.  Email:{{$data->email}} and Password: {{$data->identification}}</textarea><br>
                                                                                    <font size="1">(Maximum characters: 160)<br>
                                                                                </div>

                                                                                    You have <input readonly type="text" name="countdown" size="3" value="160"> characters left.</font>
                                                                                <input type="text" name="phonenumber" value="{{$data->telephonenumber}}" hidden>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Send
                                                                                    </button>
                                                                                </div>

                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                    </td>
                                                    <td>{{$data->typeofmembership}}</td>
                                                    <td>{{$data->chamber}}</td>
                                                    <td>{{$data->firstname}}</td>
                                                    <td>{{$data->lastname}}</td>
                                                    <td>{{$data->tinnumber}}</td>
                                                    <td>{{$data->telephonenumber}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->website}}</td>
                                                    <td>{{$data->age}}</td>
                                                    <td>{{$data->nationality}}</td>
                                                    <td>{{$data->province}}</td>
                                                    <td>{{$data->district}}</td>
                                                    <td>{{$data->sector}}</td>
                                                    <td>{{$data->cell}}</td>

                                                    <td>{{$data->workingexperience}}</td>
                                                    <td>{{$data->educationlevel}}</td>
                                                    <td>{{$data->areofinterest}}</td>
                                                    <td>{{$data->export}}</td>

                                                    <td>
                                                        <img src="passport/{{$data->attachyourpassport}}" style="width: 100px;">
                                                    </td>
                                                    <td>
                                                        <a href="cv/{{$data->attachyourcv}}" class="btn btn-secondary btn-min-width mr-1 mb-1" target="_blank"><i class="fas fa-file"></i> CV</a>
                                                    </td>
                                                    <td>
                                                        <a href="diplomas/{{$data->attachyourdiplomas}}" class="btn btn-secondary btn-min-width mr-1 mb-1" target="_blank"><i class="fas fa-file"></i> Diplomas,certificates</a>
                                                    </td>
                                                    <td>
                                                        <a href="drivinglicense/{{$data->drivinglicense}}" class="btn btn-secondary btn-min-width mr-1 mb-1" target="_blank"><i class="fas fa-file"></i> Driving License</a>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <!-- Outline Icon Button group -->
                                                            <p style="text-align: center">{{$data->approval}}</p>
                                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                                <a href="{{ route('backend.ApproveMember',['MemberId'=> $data->id,'Status'=> 'Approved'])}}" class="btn btn-icon btn-outline-secondary"><i class="fas fa-user-check"></i></a>
                                                                <a href="{{ route('backend.ApproveMember',['MemberId'=> $data->id,'Status'=> 'Denied'])}}" class="btn btn-icon btn-outline-primary"><i class="fas fa-user-times"></i></a>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td>{{$data->approved_date}}</td>
                                                    <td>{{$data->expiration_date}}</td>
                                                    <td>{{$data->identification}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                                 <?php
                                                                        $bankslip =$data->bankslip;
                                                                        $addbankslip = route('backend.AddBankSlip',['id'=> $data->id]);
                                                                    if(empty($bankslip)){
                                                                        echo "<a href='$addbankslip' class='btn btn-icon btn-outline-secondary'><i class='fas fa-plus'></i></a>";
                                                                    }else{
                                                                        echo "<a href='bankslip/$data->bankslip' class='btn btn-icon btn-outline-secondary'>View Bank Slip</a>";
                                                                    }
                                                                 ?>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td><a href="{{ route('backend.EditJoinMember',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Edit</a></td>

                                                    {{--<td>{{$data->name}}</td>--}}
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection
