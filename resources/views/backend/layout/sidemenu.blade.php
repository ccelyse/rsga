<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <?php
            $user_id = \Auth::user()->id;
            $Userrole = \App\User::where('id',$user_id)->value('role');

            $MembersProfile = url('MembersProfile');
            $MemberAccount = url('MemberAccount');
            $CreateAccount = url('CreateAccount');
            $AccountList = url('AccountList');
            $ListOfMembers = url('ListOfMembers');
            $AddNews = url('AddNews');
            $NewsList = url('NewsList');
            $SendEmail = url('SendEmail');
            $SendSMS = url('SendSMS');
            $Photocategories = url('MemberPhotocategory');
            $MemberUploadPhoto = url('MemberUploadPhoto');
            switch ($Userrole) {
                case "0":
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.templates.main'>Account</span></a> <ul class='menu-content'> <li class=' nav-item'><a href=''><i class='fas fa-user-alt'></i><span class='menu-title' data-i18n='nav.dash.main'>Create Account</span></a> </li><li class=' nav-item'><a href=''><i class='fas fa-user-circle'></i><span class='menu-title' data-i18n='nav.dash.main'>Account List</span></a> </li></ul></li>";
                    echo "<li class=' nav-item'><a href='$ListOfMembers'><i class='fas fa-users'></i><span class='menu-title' data-i18n='nav.dash.main'>Members</span></a> </li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-newspaper'></i><span class='menu-title' data-i18n=''>News</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$AddNews'><i class='fas fa-newspaper'></i><span class='menu-title' data-i18n='nav.dash.main'>Add News</span></a> </li><li class=' nav-item'><a href='$NewsList'><i class='fas fa-edit'></i><span class='menu-title' data-i18n='nav.dash.main'>News List</span></a></li></ul></li>";
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-envelope-open'></i><span class='menu-title' data-i18n=''>Communication</span></a> <ul class='menu-content'> <li class=' nav-item'><a href='$SendEmail'><i class='fas fa-envelope-open'></i><span class='menu-title' data-i18n='nav.dash.main'>Send an Email</span></a> </li><li class=' nav-item'><a href='$SendSMS'><i class='fas fa-comment'></i><span class='menu-title' data-i18n='nav.dash.main'>Send an SMS</span></a> </li></ul></li>";
                    echo "<li class=' nav-item'><a href='$Photocategories'><i class='fas fa-camera-retro'></i><span class='menu-title' data-i18n='nav.dash.main'>Photo Categories</span></a> </li>";
                    break;

                case "1":
                    echo "<li class=' nav-item'><a href='#'><i class='fas fa-chart-line'></i><span class='menu-title'>Dashboard</span></a></li>";
                    echo "<li class=' nav-item'><a href='$MemberAccount'><i class='fas fa-users'></i><span class='menu-title'>Account</span></a></li>";
                    echo "<li class=' nav-item'><a href='$MembersProfile'><i class='fas fa-address-book'></i><span class='menu-title'>Members Information</span></a></li>";
                    echo "<li class=' nav-item'><a href='$MemberUploadPhoto'><i class='fas fa-camera-retro'></i><span class='menu-title'>Photo Library</span></a></li>";

                    break;

                default:
                    return redirect()->back();
                    break;
            }
            ?>

            {{--<li class=" nav-item"><a href="#"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.templates.main">Account</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('CreateAccount')}}"><i class="fas fa-user-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Create Account</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('AccountList')}}"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.dash.main">Account List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class=" nav-item"><a href="{{url('ListOfMembers')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">Members</span></a>--}}
            {{--</li>--}}


            {{--<li class=" nav-item"><a href="#"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="">News</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('AddNews')}}"><i class="fas fa-newspaper"></i><span class="menu-title" data-i18n="nav.dash.main">Add News</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('NewsList')}}"><i class="fas fa-edit"></i><span class="menu-title" data-i18n="nav.dash.main">News List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class=" nav-item"><a href="#"><i class="fas fa-envelope-open"></i><span class="menu-title" data-i18n="">Communication</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('SendEmail')}}"><i class="fas fa-envelope-open"></i><span class="menu-title" data-i18n="nav.dash.main">Send an Email</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('SendSMS')}}"><i class="fas fa-comment"></i><span class="menu-title" data-i18n="nav.dash.main">Send an SMS</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

        </ul>
    </div>
</div>


