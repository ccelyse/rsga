@extends('layouts.master')

@section('title', 'RSGA')

@section('content')

    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
<div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/businesBisat.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style="background-color:transparent;opacity:0.5;"></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">Visit Rwanda</h1>
            </div>
        </div>
    </div>

    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-14975 page type-page status-publish hentry" id="14975">
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:30px;"></div>
                                        </div>
                                    </section>
                                    <section data-header-style="" class="row fw-row  dynamic-header-change">
                                        <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                            <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                                <section class="container " style="padding-right: 15px; padding-left: 15px">
                                                    <div class="row">
                                                        <div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                                        <div class="spb_content_element col-sm-6 spb_text_column">
                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">

                                                                <p>Rwanda is a landlocked country in Central Africa; also known as “the land of a thousand hills”.</p>
                                                                <p>Rwanda has five volcanoes, 23 lakes and numerous rivers, some forming the source of the River Nile.</p>
                                                                <p>The country lies 75 miles south of the Equator in the Tropic of Capricorn,880miles “as the crow flies” west of the Indian Ocean and 1,250 miles east of the Atlantic Ocean – literally at the heart of Africa.</p>
                                                                <p>Rwanda is bordered by Uganda to the North,Tanzania to the East, Burundi to the South and the Democratic Republic of Congo to the West.</p>

                                                                <!--																	<div class="title-wrap">-->
                                                                <!--																		<h5 class="spb-heading spb-text-heading"><span>Rwanda is an ideal all-year round tourism and MICE destination based on:</span></h5>-->
                                                                <!--																	</div>-->
                                                                <p>Rwanda is an ideal all-year round tourism and MICE destination based on:</p>
                                                                <ul class="sf-list ">
                                                                    <li><i class="sf-icon-right-chevron"></i><span>Superlative infrastructure and telecommunications capabilities</span></li>
                                                                    <li><i class="sf-icon-right-chevron"></i><span>Easy air access and a streamlined visa process</span></li>
                                                                    <li><i class="sf-icon-right-chevron"></i><span>High levels of safety and little traffic congestion</span></li>
                                                                    <li><i class="sf-icon-right-chevron"></i><span>An exceptionally clean country</span></li>
                                                                    <li><i class="sf-icon-right-chevron"></i><span>An array of iconic tourism attractions in close proximity</span></li>

                                                                </ul>
                                                                <p><strong>Time Zone:</strong>  Central African Time (CAT) – GMT +2 hours</p>
                                                                <p><strong>Size:</strong>   26,338 square kilometers</p>
                                                                <p><strong>Population:</strong>   11.46 million</p>
                                                                <p><strong>Capital City:</strong>   Kigali</p>
                                                                <p><strong>Local Currency:</strong>   Rwandan Franc</p>
                                                                <p><strong>Foreign Currencies:</strong>   All major currencies are accepted</p>
                                                                <p><strong>Official Languages:</strong>   Kinyarwanda, French, English and Kiswahili</p>
                                                                <p><strong>President:</strong>   H.E. Paul KAGAME</p>
                                                                <p><strong>Telephone Country Code:</strong>   +250 </p>
                                                                <p><strong>ICT:</strong>   High speed 4G LTE wireless broadband</p>
                                                                <p><strong>Climate:</strong>   Temperate all year round (24 – 27 degrees Celsius)</p>
                                                                <p><strong>Major Economic Sectors:</strong>   Tourism, mining and agriculture</p>

                                                            </div>
                                                        </div>
                                                        <div class="spb_content_element col-sm-6 spb_text_column">
                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1020775.8469852845!2d29.325458294444182!3d-1.9939940146731912!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1srwanda+attractions!5e0!3m2!1sen!2srw!4v1534155338696" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section class="row fw-row ">
                                                    <div class="product_list_widget woocommerce spb_content_element col-sm-12">
                                                        <div class="spb-asset-content">
                                                            <div class="title-wrap clearfix container "></div>
                                                            <ul class="products list-latest-products row products-full-width multi-masonry-items product-type-standard " data-columns="4">
                                                                <div class="clearfix product col-sm-3 grid-sizer"></div>

                                                                {{--<li class="first qv-hover product-display-gallery details-align-left col-sm-6 product" data-width="col-sm-6">--}}
                                                                    {{--<figure class="animated-overlay product-transition-zoom">--}}
                                                                        {{--<div class="badge-wrap"></div>--}}
                                                                        {{--<div class="multi-masonry-img-wrap"><img src="images/akagera.jpeg" width="800" height="650" alt="Sun Buddies 04" /></div>--}}
                                                                        {{--<div class="hover-price">--}}
                                                                            {{--<span class="price"><span class="amount">Eastern Rwanda</span></span>--}}
                                                                        {{--</div>--}}

                                                                        {{--<a href=""></a>--}}
                                                                        {{--<div class="figcaption-wrap"></div>--}}
                                                                        {{--<figcaption>--}}
                                                                            {{--<div class="thumb-info">--}}
                                                                                {{--<h4>Akagera National Park</h4>--}}
                                                                                {{--<h5 class="posted_in"><a href="" rel="tag">Eastern Rwanda</a></h5>--}}
                                                                            {{--</div>--}}
                                                                        {{--</figcaption>--}}
                                                                    {{--</figure>--}}
                                                                {{--</li>--}}
                                                                @foreach($data as $datas)
                                                                <li class="qv-hover product-display-gallery details-align-left col-sm-3 product" data-width="col-sm-3">
                                                                    <figure class="animated-overlay product-transition-zoom">
                                                                        <div class="badge-wrap"></div>
                                                                        <div class="multi-masonry-img-wrap"><img src="attractions/{{$datas->attraction_image}}"  alt="" /></div>
                                                                        <div class="hover-price">
                                                                            <span class="price"><span class="amount">{{$datas->attraction_province}}</span></span>
                                                                        </div>

                                                                        <a href="{{ route('AttractionsMore',['id'=> $datas->id])}}"></a>
                                                                        <div class="figcaption-wrap"></div>
                                                                        <figcaption>
                                                                            <div class="thumb-info">
                                                                                <h4>{{$datas->attraction_name}}</h4>
                                                                                <h5 class="posted_in"><a href="" rel="tag">{{$datas->attraction_province}}</a></h5>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>

                                                                </li>
                                                                @endforeach


                                                            </ul>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
    @include('layouts.footer')
@endsection