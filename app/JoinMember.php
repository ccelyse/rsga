<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinMember extends Model
{
    protected $table = "JoinMember";
    protected $fillable = ['id','typeofmembership','chamber','firstname','lastname','tinnumber','telephonenumber','email','website','age','nationality','province','district','sector','cell','workingexperience','educationlevel','areofinterest','attachyourpassport','attachyourcv','attachyourdiplomas','drivinglicense','criminal_record','description','identification','approval','Memberstatus','user_id','bankslip','expiration_date','approved_date'];
}
