<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCompany extends Model
{
    protected $table = "subcompanycategory";
    protected $fillable = ['id','companycategory','subcompanycategory','seatingcapacity','numberofroom','Grade'];
}
