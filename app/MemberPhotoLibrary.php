<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberPhotoLibrary extends Model
{
    protected $table = "memberphotolibrary";
    protected $fillable = ['id','photoname','phototitle','photodescription','member_id','photocategories'];
}
