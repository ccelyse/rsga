<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoCategories extends Model
{
    protected $table = "photocategories";
    protected $fillable = ['id','categoryname','categorypicture'];
}
