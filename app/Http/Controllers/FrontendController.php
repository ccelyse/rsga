<?php

namespace App\Http\Controllers;

use App\CompanyCategory;
use App\JoinMember;
use App\MemberPhotoLibrary;
use App\PhotoCategories;
use App\Post;
use App\SubCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function Home(){
        return view('welcome');
    }
    public function AboutUs(){
        return view('Aboutus');
    }
    public function BecomeMember(){
//        $listcompany = CompanyCategory::all();
        return view('BecomeMember');
    }

    public function JoinMembers(Request $request){
        $all = $request->all();
//        dd($all);
        $messages = [
            'fileToUpload' => 'The file to upload must be in PDF format',
            'attachyourcv' => 'The file to upload must be in PDF format',
            'attachyourdiplomas' => 'The file to upload must be in PDF format',
            'drivinglicense' => 'The file to upload must be in PDF format',
            'criminal_record' => 'The file to upload must be in PDF format',
        ];
        $request->validate([
            'attachyourcv'   => 'mimes:pdf',
            'attachyourdiplomas'   => 'mimes:pdf',
            'drivinglicense'   => 'mimes:pdf',
            'criminal_record'   => 'mimes:pdf',
            'attachyourpassport'   => 'mimes:jpeg,jpg,png',
        ],$messages);

        $firstname = $request['firstname'];
        $lastname = $request['lastname'];

        $JoinM = new JoinMember();

        $attachyourpassport = $request->file('attachyourpassport');
        $attachyourcv = $request->file('attachyourcv');
        $attachyourdiplomas = $request->file('attachyourdiplomas');
        $drivinglicense = $request->file('drivinglicense');
        $criminal_record = $request->file('criminal_record');

        $JoinM->typeofmembership = $request['typeofmembership'];
        $JoinM->chamber = $request['chamber'];
        $JoinM->firstname = $request['firstname'];
        $JoinM->lastname = $request['lastname'];
        $JoinM->tinnumber = $request['tinnumber'];
        $JoinM->telephonenumber = $request['telephonenumber'];
        $JoinM->email = $request['email'];
        $JoinM->website = $request['website'];
        $JoinM->age = $request['age'];
        $JoinM->nationality = $request['nationality'];
        $JoinM->province = $request['province'];
        $JoinM->district = $request['district'];
        $JoinM->sector = $request['sector'];
        $JoinM->cell = $request['cell'];
        $JoinM->workingexperience = $request['workingexperience'];
        $JoinM->educationlevel = $request['educationlevel'];
        $JoinM->areofinterest = $request['areofinterest'];
        $JoinM->description = $request['description'];
        $Memberstatus = 'Pending';
        $JoinM->Memberstatus = $Memberstatus;
        $JoinM->user_id = '1';

        $JoinM->attachyourpassport = $attachyourpassport->getClientOriginalName();
        $JoinM->attachyourcv = $attachyourcv->getClientOriginalName();
        $JoinM->attachyourdiplomas = $attachyourdiplomas->getClientOriginalName();
        $JoinM->drivinglicense = $drivinglicense->getClientOriginalName();
        $JoinM->criminal_record = $criminal_record->getClientOriginalName();

        $attachyourpassport_ = $attachyourpassport->getClientOriginalName();
        $attachyourcv_ = $attachyourcv->getClientOriginalName();
        $attachyourdiplomas_ = $attachyourdiplomas->getClientOriginalName();
        $drivinglicense_ = $drivinglicense->getClientOriginalName();
        $attachcriminal_record = $criminal_record->getClientOriginalName();

        $destinationPathpassport = public_path('/passport');
        $destinationPathcv = public_path('/cv');
        $destinationPathdiplomas = public_path('/diplomas');
        $destinationPathdrivinglicense = public_path('/drivinglicense');
        $destinationPathcriminal_record = public_path('/criminal_record');

        $attachyourpassport->move($destinationPathpassport, $attachyourpassport_);
        $attachyourcv->move($destinationPathcv, $attachyourcv_);
        $attachyourdiplomas->move($destinationPathdiplomas, $attachyourdiplomas_);
        $drivinglicense->move($destinationPathdrivinglicense, $drivinglicense_);
        $criminal_record->move($destinationPathcriminal_record, $attachcriminal_record);

        $JoinM->save();
        return view('Thankyou')->with(['firstname'=>$firstname,'lastname'=>$lastname]);
    }
    public function BusinessLicensing(){
        return view('BusinessLicensing');
    }
    public function News(){
        $newpost = Post::orderBy('created_at','desc')->get();
        return view('News')->with(['newpost'=>$newpost]);
    }
    public function NewsReadMore(Request $request){
        $id = $request['id'];
        $newpost = Post::where('id',$id)->get();
        return view('NewsReadMore')->with(['newpost'=>$newpost]);
    }
    public function Publications(){
        return view('Publications');
    }
    public function ContactUs(){
        return view('ContactUs');
    }
    public function Thankyou(){
        return view('Thankyou');
    }
    public function Login(){
        return view('backend.Login');
    }
    public function MemberDirectory(){
        $listcat = JoinMember::where('approval','Approved')->get();
        return view('MemberDirectory')->with(['listcat'=>$listcat]);
    }
    public function Webmail(){
        return redirect('https://mail.rha.rw/');
    }
    public function StockPicture(){
        $listcat = PhotoCategories::all();
        return view('StockPicture')->with(['listcat'=>$listcat]);
    }
    public function PhotoLibraryViewMore(Request $request){
        $id = $request['category'];
        $listcat = PhotoCategories::where('categoryname',$id)->get();
        $findpicture= MemberPhotoLibrary::where('photocategories',$id)->get();
        return view('PhotoLibraryViewMore')->with(['findpicture'=>$findpicture,'listcat'=>$listcat]);
    }


//    public function HotelCategory(Request $request)
//    {
//        $cate = $request['hotelcategory'];
////        $cate = 'Accomodation';
////        $cate = 'Bar';
//
//
//        if ($cate == 'Accomodation') {
//            $checksub = SubCompany::where('companycategory', $cate)->get();
////            echo "<option value=\"\" selected>Select Sub Category</option>";
////            echo "<label for=\"billing_country\" class=\"\">Company Sub Category</label><select class=\"country_to_state country_select billing_country\" name=\"hotelcategory\" id=\"hotelcategory\" required><option value=\"\">$datas->subcompanycategory</option></select>";
////            foreach ($checksub as $datas) {
////                echo "<option value=\"$datas->subcompanycategory\">$datas->subcompanycategory</option>";
////            }
//            echo "<label for=\"billing_last_name\" class=\"\">Company Sub Category</label><select class=\"form-control\" id=\"basicSelect\" name=\"subcompanycategory\" style='width:60% !important' onChange=\"getSubCat(this.value);\"><option value=\"Hotel\">Hotel</option><option value=\"Apartment\">Apartment</option><option value=\"Villa\">Villa</option><option value=\"Motel\">Motel</option><option value=\"Cottage\">Cottage</option><option value=\"Logde\">Logde</option><option value=\"Tented camp\">Tented camp</option></select>";
//
//        }elseif($cate == 'Restaurant' || $cate == 'Nightclub'|| $cate == 'Coffee shop' || $cate == 'Bar'){
////            echo "<option value=\"1 star\" selected>1 star</option>";
////            echo "<option value=\"2 star\" selected>2 star</option>";
////            echo "<option value=\"3 star\" selected>3 star</option>";
////            echo "<option value=\"4 star\" selected>4 star</option>";
////            echo "<option value=\"5 star\" selected>5 star</option>";
////
//            echo "<label for=\"billing_last_name\" class=\"\">Seating Capacity (Eg:50)</label><input type=\"text\" class=\"input-text\" name=\"seatingcapacity\" id=\"billing_last_name\" placeholder=\"\"  required/>";
////            echo "<input type=\"text\" class=\"input-text\" name=\"Seatingcapacity\" id=\"billing_last_name\" placeholder=''>";
//        }
//
//    }
//    public function SubCategory(Request $request){
//        $subcompanycategory = $request['subcompanycategory'];
//        if($subcompanycategory == 'Hotel' || $subcompanycategory == 'Apartment'|| $subcompanycategory == 'Logde'){
//            echo "<label for=\"billing_last_name\" class=\"\">Grading</label><select class=\"form-control\" id=\"basicSelect\" name=\"subcompanycategory\" style='width:60% !important;margin-bottom: 15px;' onChange=\"getCity(this.value);\"><option value=\"Hotel\">1 star</option><option value=\"Apartment\">2 star</option><option value=\"Villa\">3 star</option><option value=\"Motel\">4 star</option><option value=\"Cottage\">5 star</option></select>";
//            echo"<label for=\"billing_last_name\" class=\"\">Number of Rooms</label><input type=\"text\" class=\"input-text\" name=\"numberofrooms\" id=\"billing_last_name\" placeholder=\"\" />";
//        }
//
//    }

}
