<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attractions extends Model
{
    protected $table = "Attractions";
    protected $fillable = ['id','attraction_province','attraction_name','attraction_image','attraction_indetails'];
}
